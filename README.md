# SteamAPI Playtime Logger Backend

Find the frontend at https://gitlab.com/Mq_/steamapi-frontend

Run every hour or so to log your playtime of steam games into a database.
You need a steam API key to put requests to the steam API.