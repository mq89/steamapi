#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
Created on 07.11.2015

@author: Markus Heinrich (mh@0x25.net)
'''

import urllib2
import json
import logging
import logging.config
import MySQLdb as mdb
from datetime import datetime
from API_KEY import API_KEY

API_URL = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key={0}&steamid={1}&format=json"
LOG = None
CURSOR = None


def getSteamIDs():
    CURSOR.execute("SELECT steamid FROM player")
    ids = [col[0] for col in CURSOR.fetchall()]
    return ids


def getData(steamid):
    url = API_URL.format(API_KEY, steamid)
    try:
        response = urllib2.urlopen(url)
        result = response.read()
        response.close()
    except:
        LOG.error("Could not load page for steamid {0}".format(steamid))
        return []
    try:
        return json.loads(result)['response']
    except ValueError:
        return []


def saveRecord(steamid, record):
    saveApp(record)
    CURSOR.execute("\
            SELECT MAX(playtime)\
            FROM playtime\
            WHERE steamid = {0} AND appid = {1} LIMIT 1"
                   .format(steamid, record["appid"]))

    result = CURSOR.fetchall()

    if (len(result) == 0 or result[0][0] is None or int(result[0][0]) != int(record["playtime_forever"])):
        CURSOR.execute('''INSERT INTO playtime
                    (appid, steamid, playtime)
                    VALUES
                    ({0}, {1}, {2})
                '''.format(record["appid"],
                           steamid,
                           record["playtime_forever"]))


def saveApp(record):
    CURSOR.execute(
        "SELECT name FROM apps WHERE appid = {0}".format(record["appid"]))
    if len(CURSOR.fetchall()) == 0:
        img_logo_url = ""
        if "img_logo_url" in record:
            img_logo_url = record["img_logo_url"]
        CURSOR.execute('''INSERT INTO apps
                (appid, name, img_icon_url, img_logo_url)
                VALUES
                ({0}, "{1}", '{2}', '{3}')
            '''.format(record["appid"],
                       record["name"],
                       record["img_icon_url"],
                       img_logo_url))
        LOG.info("Inserted appid {0}".format(record["appid"]))


def printFeedback():
    if (datetime.now().hour == 4):
        if not CURSOR:
            print("Cursor ist nicht definiert!")
        else:
            try:
                CURSOR.execute(
                    "SELECT time\
                     FROM playtime\
                     WHERE id = (SELECT MAX(id) FROM playtime)")
                result = CURSOR.fetchall()
                print("Neuester Eintrag ist vom: {0}".format(result[0][0]))
            except Exception as e:
                print("Fehler bei der Abfrage!")
                print(type(e))
                LOG.error("Feedback error: {0}".format(e))


def main():
    logging.config.fileConfig('logging.conf')
    global LOG
    LOG = logging.getLogger("steamapi")
    LOG.info("Execution started")
    global CURSOR
    conn = None
    try:
        conn = mdb.connect(host="192.168.0.3", user="steamapi", db="steam")
    except mdb.OperationalError as e:
        LOG.error(e)

    if conn:
        CURSOR = conn.cursor()
        for steamid in getSteamIDs():
            response = getData(steamid)
            if not response == [] and "total_count" in response and response["total_count"] > 0:
                for record in response['games']:
                    saveRecord(steamid, record)

    # printFeedback()

    if conn:
        conn.close()
    LOG.info("Execution finished")


if __name__ == '__main__':
    main()
